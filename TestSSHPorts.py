#!/usr/bin/python
#To Do: Log server information file unavailability

import socket
import sys 
import logging
import os
#####################LOGGING DEF###############################
logger = logging.getLogger('ForensicServerTest')
hdlr = logging.FileHandler('ForensicsDest.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)
#########################################################

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print ("Socket successfully created")
except socket.error as err:
    print ("socket creation failed with error %s" %(err))
    logging.error("socket creation failed with error %s" %(err))
    

# default port for socket
port = 22

# Retreive the list of forensics servers
try:
    print("loading server file")
    with open("/Path 2/servers.txt", "r+") as f:
        if f.mode == 'r+':
            for line in f.readlines():
                try:
    
    # For every server, retreive the IP address
                    host_ip = socket.gethostbyname(line.strip())
                    print(host_ip)
    # Is server UP or DOWN
                    response = os.system('ping -i 2'" " +host_ip)
                    if response == 0:
                        print(host_ip, "is up")
                        logger.info("Forensic servers:%s" %(host_ip),"is UP") 
                    else:
                        print(host_ip, "is down")
                        logger.error("Forensic servers availability: ECHO REPLY FAILED For:%s" %(host_ip))
    # connecting to the server
                    s.connect_ex((host_ip,port))
                    print("The test has successfully reached port %s" %(port), "on host: %s" %(line) )
    #logging to a time stamped file
                #logging.info("The test has successfully reached port %s" %(port), "on host: %s" %(line) 
                except socket.gaierror:
    # This means could not resolve the host
                    print("##########################################################")
                    print("There was an error resolving the host:",line)
                    print("SSH connection to this server FAILED, please CHECK it")
                    print("##########################################################")
    #logging to a time stamped file
                    logger.error("Forensic servers availability: SSH CONNECTION FAILED FOR:%s" %(line))
#                    print("\n")

    #sys.exit()
    #servers.close()
            
        else:
            print("Server information file is not readable")
    #logging to a time stamped file
            logging.error("Forensic servers availability: Server information file is not readable")
except:
    print("Forensic servers availability: Server file is missing")
    logging.error("Forensic servers availability: Server file is missing")