#!/usr/bin/python
# TO DO Add logging

# ################################Tools###############################
#   0.User Paramiko Interactive Shell https://github.com/paramiko/paramiko/blob/master/demos/interactive.py
#   1. Install the paramikio module for python: pip install paramiko or conda install paramiko
#   2. Key file should be in pem format (ssh-keygen -m pem -t rsa -C "your user for exemple")
#   3. pip install cryptography==2.4.2 in case of authentication error

###################################### Unless you know what's going on, do not edit the SSH details below.##############################################


import paramiko
import sys
import os
import time

#######################Define Logging Infos ###############################
#import daiquiri
#daiquiri.setup()
#daiquiri.getLogger("Connect2Roofs").error("something went wrong")
###########################################################################
## Roofs Remote Servers List##
server ={
    1 : "hostname00",
    2 : "hostname01",
    3 : "hostname02",
    4 : "hostname03"
}

#################
print("Following numbers should be used:\n 1 for hostname00\n 2 for hostname01\n 3 for hostname03\n 4 for hostname04\n")

hostname = int(input("Please provide Roof number you want to connect to:"))
SSH_hostname = server.get(hostname, None)
print(SSH_hostname)

##############################
hostname = SSH_hostname
myuser = "username"
myPWD = ""
myKEY = "/Pathe 2/.ssh/id_rsa"
ssh2roof   = paramiko.SSHClient()  # Create the SSH object
ssh2roof.set_missing_host_key_policy(paramiko.AutoAddPolicy())# no known_hosts error
paramiko.util.log_to_file('Roofssh.log')  # sets up log

try:
   import interactive
   ssh2roof.connect(hostname, username=myuser, key_filename=myKEY) # no passwd needed
   print("Successfully connected to %s"%SSH_hostname)
   roofx = ssh2roof.invoke_shell()
   interactive.interactive_shell(roofx)
   ssh2roof.close()
   paramiko.util.log_to_file('/Path 2/Roofssh.log')  # sets up log

except Exception as e:
   from . import interactive
   sys.stderr.write("SSH connection error: {0}".format(e))
   paramiko.util.log_to_file('/Path 2/Roofssh.log')  # sets up log

